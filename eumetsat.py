#!/usr/bin/python3
import argparse
import subprocess
import time
from datetime import datetime
import sys
import os
import re
import threading
import queue
# import multiprocessing
import tempfile
import shutil
from urllib import request
from config import *
from pprint import pprint

# Version
PROGRAM_VERSION = '0.5'

# Global (set thru argparse)
SHOW_TIMESTAMP = None

# Number of threads to use
# NUM_THREADS = multiprocessing.cpu_count()
NUM_THREADS = 15

# --------------------------------------------------------------------------------------------

def log(msg=None, end='\n'):
  ''' Display a message with an optional timestamp '''
  if SHOW_TIMESTAMP:
    timeStr = datetime.now().strftime('%Y/%m/%d-%H:%M:%S')
    print(f"[{timeStr}] {msg if msg else ''}", end=end)
  else:
    if msg:
      print(msg, end=end)
    else:
      print()

  if end == '':
    sys.stdout.flush()

# --------------------------------------------------------------------------------------------

def checkConfig():
  ''' Check and fix user configuration if necessary '''
  global SOURCES
  global OUTPUT_FOLDER

  suffix = 'index.htm'
  for p in SOURCES:
    if p['url'].endswith(suffix):
      p['url'] = p['url'][:len(suffix)*-1]
    if not p['url'].endswith('/'):
      p['url'] += '/'
    if not p['folder'].endswith('/'):
      p['folder'] += '/'

  if not OUTPUT_FOLDER.endswith('/'):
    OUTPUT_FOLDER += '/'

# --------------------------------------------------------------------------------------------

def getSource(src):
  ''' Get the latest list of frames from a given source '''
  log(f"[{src['name']}] Getting HTML...")
  html = str(request.urlopen(src['url']).read())

  log(f"[{src['name']}] Parsing...")
  frames = {
    int(match.group(1)):
    {
      'date': match.group(2),
      'time': match.group(3),
    }
    for match in re.finditer(r'<option value="(\d+)">(\S+)   (\S+)\S*\s*', html)
  }

  images = {
    int(match.group(1)):
    {
      'name': match.group(2),
    }
    for match in re.finditer(r'array_nom_imagen\[(\d+)\]="(\S+)"\s*', html)
  }

  for id in frames:
    frames[id]['img'] = images[id]['name']

  return frames

# --------------------------------------------------------------------------------------------

def processFrames(frames, src):
  ''' Process a list of frames to generate more info '''
  log(f"[{src['name']}] Processing frames...")

  for id in frames:
    frames[id]['url'] = f"{src['url']}IMAGESDisplay/{frames[id]['img']}"

    date = frames[id]['date']
    time = frames[id]['time']
    d = f"{date[-2:]}{date[3:5]}{date[:2]}"
    t = f"{time[:2]}{time[-2:]}"
    frames[id]['file'] = f"{OUTPUT_FOLDER}{src['folder']}{d}{t}.{src['imageFormat']}"

  return frames

# --------------------------------------------------------------------------------------------

def downloadSingleFrame(url, fileName):
  # From: https://stackoverflow.com/questions/7243750/download-file-from-web-in-python-3
  # Download the file from `url` and save it locally under `file_name`:
  with request.urlopen(url, timeout=DOWNLOAD_TIMEOUT) as response, open(fileName, 'wb') as out_file:
      shutil.copyfileobj(response, out_file)

# --------------------------------------------------------------------------------------------

def frameDownloader(q, src, results):
  while True:
    try:
      frame = q.get_nowait()
    except:
      break

    result = {
      'source': src,
      'frame': frame
    }

    try:
      downloadSingleFrame(frame['url'], frame['file'])
      result['success'] = True
    except:
      result['success'] = False

    results.append(result)
    q.task_done()


# --------------------------------------------------------------------------------------------

def downloadFrames(frames, src):
  ''' Download a list of frames '''
  folder = OUTPUT_FOLDER + src['folder']
  if not os.path.exists(folder):
    log(f"[{src['name']}] Creating folder...")
    os.makedirs(folder)

  frames = { id:frames[id] for id in frames if not os.path.exists(frames[id]['file']) }

  q = queue.Queue()

  for k in frames:
    q.put(frames[k])

  threads = []
  results = []
  for i in range(NUM_THREADS):
    t = threading.Thread(target=frameDownloader, args=(q, src, results))
    t.start()
    threads.append(t)

  allFinished = False
  totFrames = len(frames)
  savedFrames = 0
  errors = 0

  while not allFinished:
    allFinished = True
    for t in threads:
      if t.isAlive():
        allFinished = False

    try:
      while len(results):
        r = results.pop()
        src = r['source']
        f = r['frame']
        success = r['success']

        if success:
          savedFrames += 1
        else:
          errors += 1

        log(f"[{src['name']}] "
            f"{'Downloaded' if success else 'FAILED'} "
            f"frame ({f['date']} {f['time']}) "
            f"[{savedFrames}/{totFrames} {100/(totFrames/savedFrames):5.2f}%]"
          )

    except:
      pass

  log(f"[{src['name']}] {savedFrames} downloaded, {errors} errors{' '*30}")

# --------------------------------------------------------------------------------------------

def updateVideo(src, args):
  ''' Update a source's video '''
  log(f"[{src['name']}] Updating video...")
  framesFolder = OUTPUT_FOLDER + src['folder']
  outFile = OUTPUT_FOLDER + src['folder'][:-1] + '.mp4'

  frames = sorted(os.listdir(framesFolder))
  vf = src['videoFrames']
  videoFrames = frames[vf*-1:]

  with tempfile.TemporaryDirectory(dir=framesFolder) as tmpDirName:
    tmpFolder = tmpDirName+'/'
    for f in videoFrames:
      os.rename(framesFolder+f, tmpFolder+f)

    cmd = []
    cmd.append('ffmpeg')
    # cmd.extend(['-pix_fmt', 'yuv420p'])
    cmd.extend(['-framerate', str(src['videoFPS'])])
    cmd.extend(['-pattern_type', 'glob'])
    cmd.extend(['-i', f"{tmpFolder}*.{src['imageFormat']}"])
    cmd.extend(['-loglevel', 'info' if args.verbose else 'error'])
    cmd.append('-y')
    cmd.append(outFile)

    log(f"[{src['name']}] {' '.join(cmd)}")

    try:
      subprocess.call(cmd)
    except:
      log(f"[{src['name']}] ERROR updating video!")

    for f in videoFrames:
      os.rename(tmpFolder+f, framesFolder+f)

# --------------------------------------------------------------------------------------------

def showLastImage(src, args):
  ''' Open last source's image '''
  log(f"[{src['name']}] Opening last image...")
  framesFolder = OUTPUT_FOLDER + src['folder']
  lastFrame = sorted(os.listdir(framesFolder))[-1:][0]

  cmd = []
  cmd.append('xdg-open')
  cmd.append(framesFolder+lastFrame)

  try:
    subprocess.call(cmd)
  except:
    log(f"[{src['name']}] ERROR playing video!")

# --------------------------------------------------------------------------------------------

def playVideo(src, args):
  ''' Play a source's video '''
  log(f"[{src['name']}] Playing video...")
  videoFile = OUTPUT_FOLDER + src['folder'][:-1] + '.mp4'

  cmd = []
  cmd.append('mplayer')
  if not args.verbose:
    cmd.append('-quiet')
    cmd.extend(['-msglevel', 'all=0'])
  if args.fullscreen:
    cmd.append('-fs')
  cmd.append(videoFile)

  try:
    subprocess.call(cmd)
  except:
    log(f"[{src['name']}] ERROR playing video!")

# --------------------------------------------------------------------------------------------

def processSources(args):
  ''' Process actions for all sources '''
  if args.download:
    for s in SOURCES:
      if s['downloadImages']:
        frames = getSource(s)
        frames = processFrames(frames, s)
        downloadFrames(frames, s)
        log()

  if args.update_videos:
    for s in SOURCES:
      if s['makeVideo']:
        updateVideo(s, args)
        log()

  if args.show_images:
    for s in SOURCES:
      if s['downloadImages']:
        showLastImage(s, args)
        log()

  if args.play_videos:
    for s in SOURCES:
      if s['makeVideo']:
        playVideo(s, args)
        log()

# --------------------------------------------------------------------------------------------

def main():
  ''' Program entry point '''
  global SHOW_TIMESTAMP

  parser = argparse.ArgumentParser()
  parser.add_argument('-d', '--download',      action='store_true', help='download frames')
  parser.add_argument('-u', '--update-videos', action='store_true', help='update videos')
  parser.add_argument('-p', '--play-videos',   action='store_true', help='play videos')
  parser.add_argument('-i', '--show-images',   action='store_true', help='show last images')
  parser.add_argument('-f', '--fullscreen',    action='store_true', help='play videos in full screen mode')
  parser.add_argument('-v', '--verbose',       action='store_true', help='set verbose mode (only for video)')
  parser.add_argument('-t', '--timestamp',     action='store_true', help='display timestamps for logging')
  args = parser.parse_args()

  # Assign to global (sorry) so log() can access it
  SHOW_TIMESTAMP = args.timestamp

  # Check if some args are present
  someArgs = False
  for k in vars(args):
    if getattr(args, k):
      someArgs = True
      break

  programStartTime = time.time()
  log(f"EUMETSAT web image scraper v{PROGRAM_VERSION}")
  log()

  if someArgs:
    checkConfig()
    processSources(args)
    log(f"Finished in {time.time() - programStartTime:.2f} seconds")
  else:
    parser.print_help()

# --------------------------------------------------------------------------------------------

if __name__ == '__main__':
  if DEBUG:
    # Let exceptions flow...
    main()
  else:
    try:
      main()
    except:
      log()
      log()
      log('----------------------------------------------')
      log('An error happened, execution interrupted:')
      log(sys.exc_info())
