# eumetsat.py

Python3 utility to gather images from [EUMETSAT's](https://www.eumetsat.int/website/home/index.html) [OIS website](http://oiswww.eumetsat.org/) and make videos from the downloaded images.

The video features are based upon `ffmpeg` and `mplayer`, so they will only run in POSIX systems.

## Setup

* Make sure you have [Python 3](https://www.python.org/downloads/) installed
* Clone this repo or download and extract the .zip file
* Copy `config-sample.py` as `config.py` and edit the new file if desired

## Configuration

The provided sample configuration looks as follows:
```
SOURCES = [
  { 'name': 'MPE Western Europe',
    'url': 'http://oiswww.eumetsat.org/IPPS/html/MSG/PRODUCTS/MPE/WESTERNEUROPE/index.htm',
    'folder': 'mpe-we',
    'imageFormat': 'png',
    'videoFrames': 100,
    'videoFPS': 20,
    'downloadImages': True,
    'makeVideo': True,
  },
  { 'name': 'MPE Full Disc High Resolution',
    'url': 'http://oiswww.eumetsat.org/IPPS/html/MSG/PRODUCTS/MPE/FULLRESOLUTION/',
    'folder': 'mpe-fd-hr',
    'imageFormat': 'png',
    'videoFrames': 10,
    'videoFPS': 2,
    'downloadImages': False,
    'makeVideo': False,
  },
  { 'name': 'MPE Full Disc Low Resolution',
    'url': 'http://oiswww.eumetsat.org/IPPS/html/MSG/PRODUCTS/MPE/FULLDISC/',
    'folder': 'mpe-fd-lr',
    'imageFormat': 'png',
    'videoFrames': 100,
    'videoFPS': 20,
    'downloadImages': True,
    'makeVideo': True,
  },
  { 'name': 'NATURALCOLOR Full Disc Low Resolution',
    'url': 'http://oiswww.eumetsat.org/IPPS/html/MSG/RGB/NATURALCOLOR/FULLDISC/',
    'folder': 'nc-fd-lr',
    'imageFormat': 'jpg',
    'videoFrames': 50,
    'videoFPS': 10,
    'downloadImages': True,
    'makeVideo': True,
  },
]

OUTPUT_FOLDER = './images'
DOWNLOAD_TIMEOUT = 5
DEBUG = False

```

### `SOURCES`
This is the list of image streams to be downloaded. Each one specifies:

* `name`: Display name
* `url`: URL
* `folder`: Name of the local folder (below `OUTPUT_FOLDER`) for this source
* `imageFormat`: Format of the images in this source
* `videoFrames`: Number of frames for the generated video
* `videoFPS`: Framerate for the generated video
* `downloadImages`: Download images from this source?
* `makeVideo`: Make a video from this source?

### `OUTPUT_FOLDER`
This is the base folder where the images and videos will be downloaded

### `DOWNLOAD_TIMEOUT`
This is the number of seconds considered to be the timeout for a frame download

### `DEBUG`
Set to `True` to enable debug mode


## Usage

```
$ python eumetsat.py
EUMETSAT web image scraper

usage: eumetsat.py [-h] [-d] [-u] [-p] [-i] [-f] [-v] [-t]

optional arguments:
  -h, --help           show this help message and exit
  -d, --download       download frames
  -u, --update-videos  update videos
  -p, --play-videos    play videos
  -i, --show-images    show last images
  -f, --fullscreen     play videos in full screen mode
  -v, --verbose        set verbose mode (only for video)
  -t, --timestamp      display timestamps for logging
```

### Easy startup sequence

First time you run *eumetsat*, you will want to download all available images and build their videos:

```
$ python eumetsat.py -du
```

After that you may want to see them (fullscreen in this example):

```
$ python eumetsat.py -pf
```

To open the last image from each source:

```
$ python eumetsat.py -i
```

Then, you can `eumetsat -d` periodically to keep your image list updated and `eumetsat -u` when you want to update the videos before a `eumetsat -p`
